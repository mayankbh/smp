#ifndef SMP_H
#define SMP_H

#define ETH_SMP_TYPE 0x88B5
#define ETH_SDS_TYPE 0x88B6

#define SUCCEEDED 0
#define FAILED -1
#define DUPLICATE 2

//specifies the role a communication endpoint is likely to play
typedef enum _role 
{
    CLIENT = 0,
    PEER = 1,
    SERVER = 2,
    TRACKER = 3,
    PROXY = 4
} role;
//each SMP end point is identified by an application level−unique serviceID
//and the role played by the end point
struct smp_ep 
{
    role r;
    char serviceID[10];
};
//each SMP end point is temporarily mapped to a socket−like combination of
//<ETH address, port>
struct smp_ep_eth 
{
    char eth_addr[6];
    uint32_t port;
};

//Each entry consists of 
struct sds_entry {
	struct smp_ep request;
	struct smp_ep_eth response;
	unsigned short int ttl;
};

typedef struct _pmcacheentry {
	role r;
	char serviceID[10];
	char eth_addr[6];
	unsigned int port;
	unsigned short int ttl;
	short int valid;
	char auth; //Marks whether the entry is valid or not.
	unsigned int timestamp;
} c_entry;

#endif
