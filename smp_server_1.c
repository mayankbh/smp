#include<stdio.h>
#include<string.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <time.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <netinet/ether.h>
#include <netinet/in.h>
#include <stdint.h>
#include <unistd.h>
#include <linux/if.h>
#include <fcntl.h>
#include <errno.h>

#include"SMP.h"
#include"SMP_library.h"

#define BUFFER_SIZE 1024
#define PORT 32
#define IF "eth0"
int main()
	{

	struct smp_ep end_p;
	int smpfd;
	char buffer[BUFSIZ];
	strncpy(end_p.serviceID,"helloworld",10);
	end_p.r = SERVER;
	struct smp_ep client_ep;
	struct ifreq if_idx;
	struct ifreq if_mac;  
	unsigned char my_mac[6];
	//smpfd = //socket_smp(&end_p,sizeof(struct smp_ep));

	smpfd = socket(AF_PACKET,SOCK_RAW,htons(0x88b5));

	/* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, IF, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, IF, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Get own MAC address
	my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];

	pm_add_entry(end_p.serviceID,SERVER,my_mac,PORT,50,1); //Adding server entry to EP port mapper

	while(1){

		memset(buffer,0,BUFFER_SIZE);

		int bytes_rcvd = recvfrom_smp_server(smpfd,buffer,BUFFER_SIZE,PORT, &client_ep);

		printf("Message received: %s\n",buffer);

		sendto_smp_server(smpfd,buffer,bytes_rcvd, PORT, &client_ep);

	}




}
