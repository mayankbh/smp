#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
int main()
{
    int fd;
    char * myfifo = "/tmp/pm_input";
    char msg[100];
	char *temp = "/tmp/pm_output";
	char buf[100];
	int rfd;
    /* create the FIFO (named pipe) */
    //mkfifo(myfifo, 0666);
	char SID[11];
	unsigned short int role;
	char eth[7];
	unsigned int port;
	unsigned short int ttl;
	unsigned short int auth;
	int choice;
	
    mkfifo("/tmp/pm_input", 0666);
    while(1)
    {
  	 fd = open(myfifo, O_WRONLY);
   	 memset(msg, 0, 100);
   	 printf("1:Lookup, 2:Insert, 3:Delete\n");
   	 scanf("%d", &choice);
	if(choice == 2)
	{
		printf("SID:");
		scanf("%s",SID);
		printf("Role:");
		scanf("%hu", &role);
		printf("Eth:");
		scanf("%s", eth);
		printf("Port:");
		scanf("%u", &port);
		printf("TTL:");
		scanf("%hu", &ttl);
		printf("Auth:");
		scanf("%hu", &auth); 
		memcpy(msg, "b", 1);
		memcpy(msg+1, (char *)&role, 2);
		memcpy(msg+3, SID , 10);
		memcpy(msg+13, eth, 6);
		memcpy(msg+19, (char*)&port, 4);
		memcpy(msg+23, (char*)&ttl, 2);
		memcpy(msg+25, (char *)&auth, 1);
		write(fd, msg, 26);
	}
	else if(choice == 1)
	{
		printf("SID:");
		scanf("%s",SID);
		printf("Role:");
		scanf("%hu", &role);
		memcpy(msg, "a", 1);
		memcpy(msg+1, SID, 10);
		memcpy(msg+11, (char *)&role, 2);
		write(fd, msg, 13);
	}
	else if(choice == 3)
	{
		printf("SID:");
		scanf("%s",SID);
		printf("Role:");
		scanf("%hu", &role);
		memcpy(msg, "c", 1);
		memcpy(msg+1, SID, 10);
		memcpy(msg+11, (char *)&role, 2);
	  	write(fd, msg, 13);	
	}

  	  close(fd);
  	  sleep(1);
  	  rfd = open(temp, O_RDONLY);
  	  memset(buf, 0, 100);
  	  read(rfd, buf, 100);
 	   close(rfd);
	    printf("Received reply %s\n", buf);
	}
    /* remove the FIFO */
    unlink(myfifo);

    return 0;
}
