#include <stdio.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <stdint.h>

#include "SMP.h"

#define SERVER_MAC0	0x50
#define SERVER_MAC1	0xb7
#define SERVER_MAC2	0xc3
#define SERVER_MAC3	0x95
#define SERVER_MAC4	0xb4
#define SERVER_MAC5	0x3a
#define SERVER 2
#define DEFAULT_IF	"eth0"
#define BUF_SIZ		1024
 
int main(int argc, char *argv[])
{
	unsigned char mymac[6] = {0xd4,0xbe,0xd9,0x46,0xce,0x8d};
	
	struct sds_entry sds_list[100];
	sds_list[0].request.r = SERVER;
	strcpy(sds_list[0].request.serviceID,"dummycheck");
	strcpy(sds_list[0].response.eth_addr,"b32112");
	sds_list[0].response.port = 50;
	sds_list[0].ttl = 10;

	sds_list[1].request.r = SERVER;
	strcpy(sds_list[1].request.serviceID,"helloworld");
	strcpy(sds_list[1].response.eth_addr,mymac);
	sds_list[1].response.port = 32;
	sds_list[1].ttl = 600;

	char sender[INET6_ADDRSTRLEN];
	int sockfd, ret, i;
	int sockopt;
	ssize_t numbytes;
	struct ifreq ifopts;	/* set promiscuous mode */
	struct sockaddr_storage their_addr;
	uint8_t buf[BUF_SIZ];
	char ifName[IFNAMSIZ];
	
	/* Get interface name */
	if (argc > 1)
		strcpy(ifName, argv[1]);
	else
		strcpy(ifName, DEFAULT_IF);
 
	/* Header structures */
	struct ether_header *eh = (struct ether_header *) buf;
 
	char * payload = buf + 14;

	/* Open PF_PACKET socket, listening for EtherType ETHER_TYPE */
	if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_SDS_TYPE))) == -1)
	{
		perror("Error creating socket.");	
		return -1;
	}

	
 
 	/* Set interface to promiscuous mode - do we need to do this every time? */
	memcpy(ifopts.ifr_name, ifName, IFNAMSIZ-1);
	ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
	ifopts.ifr_flags |= IFF_PROMISC;
	ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
	
	/* Allow the socket to be reused - incase connection is closed prematurely */
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1) 
	{
		perror("setsockopt");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	/* Bind to device */
	if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ-1) == -1)	
	{
		perror("SO_BINDTODEVICE");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
 
	//send_response("342111209123123123121221",sockfd);

	repeat: memset(buf,0,BUF_SIZ);	
	printf("Waiting for SDS request frame...\n");
	numbytes = recvfrom(sockfd, buf, BUF_SIZ, 0, NULL, NULL);
	printf("Received request frame : %lu bytes\n", numbytes);
 
	/* Check the packet is for me */
	if (eh->ether_dhost[0] == mymac[0] &&
			eh->ether_dhost[1] == mymac[1] &&
			eh->ether_dhost[2] == mymac[2] &&
			eh->ether_dhost[3] == mymac[3] &&
			eh->ether_dhost[4] == mymac[4] &&
			eh->ether_dhost[5] == mymac[5]) 
	{
		printf("SDS Server Received Frame\n");
	} 
	else 
	{
		printf("Wrong destination MAC: %x:%x:%x:%x:%x:%x\n",
						eh->ether_dhost[0],
						eh->ether_dhost[1],
						eh->ether_dhost[2],
						eh->ether_dhost[3],
						eh->ether_dhost[4],
						eh->ether_dhost[5]);
		ret = -1;
		goto done;
	}

	unsigned short int request_len = ntohs(*((short int *) (payload+2))); // Retreiving Request Length from PDU

	if(request_len != 12)
	{
		perror("Invalid length for request PDU.");
		goto done;
	} 

	char service_id[6];
	
	memcpy(service_id,payload+8,6); //Retrieving Service ID from PDU

	unsigned int role = ntohs(*((short int *) (payload + 18))); // Retrieving Role from PDU

	if(role != 1 && role !=2 && role != 3 && role != 4)
	{
		perror("Invalid value of Role field");
		goto done;
	}

	for(i=0;i<100;i++)
	{
		if(sds_list[i].request.r == role && (strncmp(service_id,sds_list[i].request.serviceID,6) == 0))
		{
			// Network Byte Order might be different.Check later.
			char response[12];
			printf("Match found\n");
			memcpy(response,sds_list[i].response.eth_addr,6); //Setting Ethernet Address
			*((uint16_t *) (response + 6)) = htons(sds_list[i].ttl);// Setting TTL
			*((uint32_t *) (response + 8)) = htonl(sds_list[i].response.port);
			//memcpy(response+8,(char *)&sds_list[i].response.port,4); // Setting Port
			memcpy(payload+20,response,12); // Appending to original request
			*((short int *)(payload + 2)) = htons(24); //Changing the length value
			break;
		}
	}

	char temp[6];

	memcpy(temp,buf,6);	//
	memcpy(buf,buf+6,6);   // Switching the source and destination MAC addresses.
	memcpy(buf+6,temp,6);  //
	
	send_response(buf,sockfd);


done: 	goto repeat;
 
	close(sockfd);
	return ret;
}

int send_response(char *frame,int sockfd)
{
	struct ifreq if_idx;
	struct ifreq if_mac;
	int tx_len = 0;
	struct sockaddr_ll socket_address;
	char ifName[IFNAMSIZ];
	
	/* Get interface name */
	strcpy(ifName, DEFAULT_IF);
 
	/* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	memcpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	memcpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");

	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address.sll_addr[0] = *frame; //
	socket_address.sll_addr[1] = *(frame+1); //
	socket_address.sll_addr[2] = *(frame+2); //
	socket_address.sll_addr[3] = *(frame+3); //
	socket_address.sll_addr[4] = *(frame+4); //
	socket_address.sll_addr[5] = *(frame+5); //
 	
	int temp;

	/* Send packet */
	if ((temp = sendto(sockfd, frame, 46, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll))) < 0)
	    printf("Send failed\n");

	printf("Data Sent: %d\n",temp);

 
	return 0;

}
