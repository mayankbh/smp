#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include "SMP.h"
#include "SMP_library.h"

int main()
{
	init_random_generator();
	char msg[100];
	char reply[100];
	struct smp_ep server;
	strcpy(server.serviceID, "helloworld");
	server.r=SERVER;
	struct smp_ep_eth server_eth;
	printf("Attempting to create socket\n");
	int smpfd = socket_smp(&server, sizeof(server));
	while(1)
	{
		memset(msg, 0, 100);
		memset(reply, 0, 100);
		printf("Enter echo input:\n");
		scanf("%s", msg);	
		sendto_smp(smpfd, msg, strlen(msg), &server, sizeof(server));
		sleep(1);
		recvfrom_smp(smpfd, reply, 100, &server, NULL);
		printf("Received : %s\n", reply);
	}
	
}
