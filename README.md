A basic implementation of an SMP client-server application (echo server)

1)Client
	a)In SMP_library.c, in the function directory_smp(), change the hardcoded SDS server MAC to that of the server.
	b)Run 'make'
	c)./portmapper.out
	d)sudo ./client.out
2)Server
	a)Configure sds.c with the MAC address of the SDS server (in the array mymac[])
	b)Add the SDS entries to the sds_list[] array
	c)Run 'make'
	d)sudo ./sds.out
	e)./portmapper.out
	f)sudo ./server.out

Ensure the server is running before the client.

Notes

The current implementation uses primitive data structures like arrays to store mappings. This could be replaced with a chained hashtable for faster lookup.
At a time, only one process should attempt to lookup/modify the EP port mapper. This can be overcome by using appropriate synchronization constructs like mutexes/semaphores.
No timeout has been implemented yet.
Applications are expected to remove the authoritative entries in the EP port mapper by using the pipe appropriately. A wrapper function can be implemented to abstract this notion.

