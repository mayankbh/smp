#include <fcntl.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include "SMP.h"
#include <string.h>

#define MAX_BUF 1024
#define CACHE_SIZE 100

#define DUPLICATE_EP	-1
#define DUPLICATE_PORT	-2

struct _pmcacheentry c_list[CACHE_SIZE];
char mymac[6] = {0xd4, 0xbe,0xd9, 0x46,0xce,0x8d};		//TODO Patch this in later

/*void pm_printcache(){
	for(i=0;i<CACHE_SIZE;i++){
		if(c_list[i].valid == 1){
			printf("Entry No: %d\n",i);
			printf("Role : %d\n",c_list[i].r);
			printf("Service ID : %s\n",c_list[i].serviceID);
			printf("Eth Address : %x:%x:%x:%x:%x:%x\n",c_list[i].eth_addr[0],c_list[i].eth_addr[1],c_list[i].eth_addr[2],c_list[i].eth_addr[3],c_list[i].eth_addr[4],c_list[i].eth_addr[4]);
			printf("TTL : %d\n".c_list[i].ttl);
			printf("Authorative: %c\n",c_list[i].auth);
		}

}*/

//This function invalidates all cache entries that have timed out
//Returns the no. of invalidated entries		
int invalidate_timed_out()
{
	int count;
	int i;

	for(i=0;i<CACHE_SIZE;i++)
	{
		if((unsigned int) time(NULL) - c_list[i].timestamp > c_list[i].ttl && c_list[i].auth == 0)
		{
			c_list[i].valid = 0;
			count++;
		}
	}

	return count;
}		


int add_entry(role r,char serviceID[10],
	char eth_addr[6], unsigned int port, 
	unsigned short int ttl,char auth)
{
	invalidate_timed_out();
	int i;
	for(i=0;i<CACHE_SIZE;i++)
	{
		//Condition for conflicting EPs
		if(c_list[i].valid == 1 && 
			c_list[i].r == CLIENT && 			//Roleof client
			(memcmp(c_list[i].eth_addr,eth_addr,6) == 0) && 
			(memcmp(c_list[i].eth_addr,mymac,6) == 0) &&	//On local system
			(memcmp(c_list[i].serviceID, serviceID, 10) == 0))	//With same service ID
		{
			return DUPLICATE_EP;	//Two clients attempting to connect to same EP
		}
		else if(c_list[i].valid == 1 &&
			c_list[i].port == port &&			//Same port
			(memcmp(c_list[i].eth_addr,eth_addr,6) == 0) && 
			(memcmp(c_list[i].eth_addr,mymac,6) == 0))	//On local system
		{
			//Duplicate port usage
			return DUPLICATE_PORT;
		}

	}

	for(i=0;i<CACHE_SIZE;i++)
	{
		if(c_list[i].valid == 0)
		{
			c_list[i].valid = 1;
			memcpy(c_list[i].eth_addr,eth_addr,6);
			c_list[i].r = r;
			memcpy(c_list[i].serviceID,serviceID,10);
			c_list[i].port = port;
			c_list[i].ttl = ttl;
			int timestamp = (unsigned int) time(NULL);
			c_list[i].timestamp = (unsigned int) time(NULL);
			return 0;
		}
	}
	return -1;

}

int remove_entry(char serviceID[10], uint16_t role)	//Delete entry matching given EP
{
	int i;
	for(i=0;i<100;i++)
	{
		
		if(c_list[i].valid==1 &&		//Valid entry
		 (memcmp(serviceID,c_list[i].serviceID, 10)==0) && 	//Matching SID	
		 role == c_list[i].r)		//Matching role
		{
			c_list[i].valid = 0;
			return 0;
		}
	}
	return -1;
}




//Checks the port mapper cache for a valid <service_id,role> pair
//IF cache hit endpoint points to the smp_ep_eth and function returns 0
//IF cache miss returns -1
int request(char serviceID[10], unsigned short int role, struct smp_ep_eth * endpoint)
{
	invalidate_timed_out();
	int i;
	for(i=0;i<100;i++){
		if((memcmp(c_list[i].serviceID,serviceID,10)==0) && 
			c_list[i].r == role && c_list[i].valid == 1)
		{
			memcpy(endpoint->eth_addr,c_list[i].eth_addr,6);
			endpoint->port = c_list[i].port;
			return 0;
		}
	}
	return -1;
}

int main()
{
	int i;

	for (i=0;i<100;i++)
	{
		c_list[i].valid = 0;	//Invalidate all entries initially
	}

	int read_fd;
	int write_fd;
    char buf[MAX_BUF];
 	char msg[11];
 	char sid[10];
 	unsigned short int role;
 	char ser_id[10];
	char eth_addr[6];
	char auth;
	unsigned int port;
	unsigned short int ttl;

	
	mkfifo("/tmp/pm_output", 0666);
	mkfifo("/tmp/pm_input", 0666);
 	

 	//read(read_fd, buf, MAX_BUF);
    
    while(1)
    {
    	/* open, read, and display the message from the FIFO */
	   	memset(buf,0,MAX_BUF);
	   	read_fd = open("/tmp/pm_input", O_RDONLY);
	    read(read_fd, buf, MAX_BUF);

	    //This clause is for cache request messages (a)
	    if(buf[0]=='a')
	    {
	    	printf("Received lookup request\n");
	    	
	    	memcpy(sid,buf+1,10);
	    	role = *((unsigned short int *)(buf+11));
	    	struct smp_ep_eth end_p;
	    	int err = request(sid,role,&end_p);
		sleep(1);
	    	write_fd = open("/tmp/pm_output", O_WRONLY);

	    	//In case of cache miss
	    	if(err == -1)
	    	{
	    		memset(msg,0,11);
	    		memcpy(msg,"n",1);
	    		write(write_fd, msg,11);
	    	} 
	    	else 
	    	{ //In case of cache hit 
	    		memset(msg,0,11);
	    		memcpy(msg,"y",1);
	    		memcpy(msg+1,(char *) &end_p.eth_addr,6);
	    		memcpy(msg+7,(char *) &end_p.port,4);
	    		write(write_fd, msg, 11);

	    	}

	    	printf("Responded to request\n");
	    
	    } 
	    else if(buf[0]=='b')
	    { //This clause is for adding new entries to the cache (b)
   	    	printf("Received insertion request\n");
   	    	
	    	role = *((short int *) (buf+1));
	    	memcpy(ser_id,buf+3,10);
	    	memcpy(eth_addr,buf+13,6);
	    	port = *((unsigned int *) (buf +19));
	    	ttl = *((unsigned short int *) (buf + 23));
	    	auth = *((char *) (buf + 25));
	    	int err = add_entry(role,ser_id,eth_addr,port,ttl,auth);

		printf("Attempting to insert role:%hu, port:%u, ttl:%hu, auth:%c\n", role, port, ttl, auth);
		sleep(1);
	    	write_fd = open("/tmp/pm_output", O_WRONLY);

	    	if(err == DUPLICATE_PORT)
	    	{
	    		printf("Duplicate port found\n");
	    		memset(msg,0,11);
	    		memcpy(msg,"n",1);
	    		write(write_fd, msg, 1);
	    	} 
	    	else if(err == 0) 
	    	{
	    		printf("Successfully inserted\n");
	    		memset(msg,0,11);
	    		memcpy(msg,"y",1);
	    		write(write_fd, msg, 1);
	    	}
	    	else if(err == DUPLICATE_EP)
	    	{
	    		printf("Duplicate EP found\n");
	    		memset(msg, 0, 11);
	    		memcpy(msg, "p", 1);
	    		write(write_fd, msg, 1);
	    	}
	    }
	    else if(buf[0] == 'c')	//To remove entries from the port mapper
	    {
	    	printf("Received deletion request\n");
	    	
	    	write_fd = open("/tmp/pm_output", O_WRONLY);
	    	close(write_fd);	
		memcpy(sid,buf+1,10);
	    	role = *((unsigned short int *)(buf+11));
	    	remove_entry(sid, role);
	    }
	    sleep(1);
	    close(read_fd);
	    close(write_fd);
    }


}
