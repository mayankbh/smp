CC_FLAGS= -g 

all: portmapper sds client server
	
portmapper:	portmapper.o
	gcc portmapper.o -o portmapper.out

sds:	sds.o
	gcc sds.o -o sds.out


client:	client.o SMP_library.o
	gcc client.o SMP_library.o -o client.out
	
server: smp_server_1.o SMP_library.o
	gcc smp_server_1.o SMP_library.o -o server.out

SMP_library.o: SMP_library.c SMP_library.h SMP.h
	gcc $(CC_FLAGS) -c SMP_library.c
		
smp_server_1.o: smp_server_1.c SMP_library.h SMP.h
	gcc -c smp_server_1.c

client.o: client.c SMP.h SMP_library.h
	gcc $(CC_FLAGS) -c client.c
		
portmapper.o: portmapper.c SMP.h
	gcc $(CC_FLAGS) -c portmapper.c

sds.o: sds.c SMP.h
	gcc $(CC_FLAGS) -c sds.c

	
	
