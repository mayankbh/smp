#include <sys/types.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <time.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <netinet/ether.h>
#include <netinet/in.h>
#include <stdint.h>
#include <unistd.h>
#include <linux/if.h>
#include <fcntl.h>
#include <errno.h>

#include "SMP.h"
#include "SMP_library.h"
	
#define ifName "eth0"

void init_random_generator()    //Initialize the random number generator, 
                                //needed for random port number generation at 
                                //client side
{
    srand(time(NULL));
}

int pm_lookup(const struct smp_ep *ep, socklen_t addrlen,
		struct smp_ep_eth *ep_addr, socklen_t ethlen)	//Checks for cached entries in EP mapper, 0 if uncached, 1 if cached                    
{
	char *r_file = "/tmp/pm_output";
	char *w_file = "/tmp/pm_input";
	int read_fd, write_fd;

	
	//TODO use mutex to ensure safety, only one process should lookup at a time
	
	write_fd = open(w_file, O_WRONLY);
	
	char buf[100];	//Request
	char msg[20];	//Response
	
	memset(buf, 0, 100);
	//Build request message in buf
	buf[0] = 'a';	//Lookup type request
	memcpy(buf+1, ep->serviceID, 10);	//Append SID
	memcpy(buf+11, (char *)&ep->r, 2);	//Role
	//Write it to FD
	
	write(write_fd, buf, 13);	
	
	
	sleep(1);
	read_fd = open(r_file, O_RDONLY);
	//Now wait for a response
	while(1)
	{
		memset(msg, 0, 20);
		int bytes_read = read(read_fd, msg, 11);
		if(msg[0] == 'n')	//Not cached
			return FAILED;
			
		else if(msg[0] == 'y')
		{
			memcpy(ep_addr->eth_addr, msg + 1, 6);
			memcpy((char *)&ep_addr->port, msg + 7, 4);	//Copy port and MAC from response
			return SUCCEEDED;	//Cached
		}			
	}	
	//TODO Finally release the mutex so other processes can use
}

int pm_add_entry(const char *serviceID, uint16_t role,  uint8_t eth_addr[6], uint32_t port, uint16_t TTL, char auth)	//Inserts entry with required mappings, returns 0 on success 
{
	char *r_file = "/tmp/pm_output";
	char *w_file = "/tmp/pm_input";
	int read_fd, write_fd;
	sleep(1);
	
	//TODO use mutex to ensure safety, only one process should lookup at a time
	
	printf("Attempting to open pipe to write\n");
	write_fd = open(w_file, O_WRONLY);
	
	char buf[100];	//Request
	char msg[20];	//Response
	
	memset(buf, 0, 100);
	//Build request message in buf
	buf[0] = 'b';	//Add type request
	memcpy(buf+1, (char *)&role, 2);
	memcpy(buf+3, serviceID, 10);
	memcpy(buf+13, eth_addr, 6);
	memcpy(buf+19, (char *)&port, 4);
	memcpy(buf+23, (char *)&TTL, 2); 
	memcpy(buf+25, (char *)&auth, 1);
	//Write it to FD
	
	write(write_fd, buf, 26);	
	
	sleep(1);
	printf("Attempting to open pipe to read\n");
	read_fd = open(r_file, O_RDONLY);
	//Now wait for a response
	while(1)
	{
		memset(msg, 0, 20);
		int bytes_read = read(read_fd, msg, 11);
		if(msg[0] == 'n')	//Unsuccessful add due to duplicate port
			return FAILED;
			
		else if(msg[0] == 'y')	//Succesfully added
		{
			return SUCCEEDED;	//Success
		}
		else if(msg[0] == 'p')	//Duplicate end point usage on client side
		{
			return DUPLICATE;
		}			
	}	
	//TODO Finally release the mutex so other processes can use	
}

int directory_smp(const struct smp_ep *ep, socklen_t addrlen,
                    char *smp_ep_eth, socklen_t ethlen)  //Returns: 0 on success, −1 on error
{
	struct ifreq if_idx;
	struct ifreq if_mac; 
	uint8_t my_mac[6];
	    int total_size = 34;		//Size of SDS request

       int sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_SDS_TYPE));
 
        
	unsigned char *sendbuf = (unsigned char *) malloc(34);		//Allocate 20 bytes for SDS request, 14 for Eth header
	unsigned char *payload = sendbuf + 14;
    /* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Hard code the SDS server for now
	uint8_t dest_mac[6] = {0xd4, 0xbe, 0xd9, 0x2e, 0x50, 0x77};	    
	    
	int TID = rand() % 	4294967296;
	//Get own MAC address
   	my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];

	memcpy(((unsigned char *)sendbuf), dest_mac, 6);	//Set Destination MAC
	memcpy(((unsigned char *)sendbuf) + 6, my_mac, 6);	//Set Source MAC
//	*((uint16_t *) (sendbuf + 12)) = htons(0x88b6);		//Set ethernet payload type
	sendbuf[12] = 0x88;
	sendbuf[13] = 0xb6;
	payload[0] = 0;	//Version
	payload[1] = 0;	//Type
	*((uint16_t *) (payload + 2)) = htons(12);	//Request length = 12
	*((uint32_t *) (payload + 4)) = TID;	//Transaction ID
	memcpy(payload + 8, ep->serviceID, 10);	//Service ID
	*((uint16_t *) (payload + 18)) = htons(ep->r);

		
	 /* Destination address */
    struct sockaddr_ll socket_address;
    
    /* Index of the network device */
    socket_address.sll_ifindex = if_idx.ifr_ifindex;
    /* Address length*/
    socket_address.sll_halen = ETH_ALEN;
    /* Destination MAC */
    socket_address.sll_addr[0] = dest_mac[0];
    socket_address.sll_addr[1] = dest_mac[1];
    socket_address.sll_addr[2] = dest_mac[2];
    socket_address.sll_addr[3] = dest_mac[3];
    socket_address.sll_addr[4] = dest_mac[4];
    socket_address.sll_addr[5] = dest_mac[5];
    /* Send packet */
    if ((sendto(sockfd, sendbuf, total_size , 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll))) < 0)
    {
	printf("Send failed\n");
	free(sendbuf);	//Don't need this buffer anymore
	return FAILED;
    }
    else       	 
	printf("Sent directory request\n");
	
	free(sendbuf);	//Don't need this buffer anymore
	//Now we wait for a reply

	
	unsigned char *receivebuf = (unsigned char *) malloc(46);	//Request should be 46 bytes
	int received;
	sleep(1);	//Wait for a while
	
	while(1)
	{
		memset(receivebuf, 0, 46);	//Clear buffer
		received = recvfrom(sockfd, receivebuf, 46, 0, NULL, NULL);
		printf("Received %d bytes\n", received);
		if(received != 46)	//Wrong length, ignore
		{
			fprintf(stderr, "Wrong length for SDS reply, ignore\n");
			continue;
		}
		if(receivebuf[0] != my_mac[0] ||
			receivebuf[1] != my_mac[1] ||
			receivebuf[2] != my_mac[2] ||
			receivebuf[3] != my_mac[3] ||
			receivebuf[4] != my_mac[4] ||
			receivebuf[5] != my_mac[5])	//Not meant for me, ignore
		{
			fprintf(stderr, "Packet not meant for me\n");
			continue;
		}
		if(receivebuf[6] != dest_mac[0] ||
			receivebuf[7] != dest_mac[1] ||
			receivebuf[8] != dest_mac[2] ||
			receivebuf[9] != dest_mac[3] ||
			receivebuf[10] != dest_mac[4] ||
			receivebuf[11] != dest_mac[5])
		{
			fprintf(stderr, "Packet not from SDS server, ignore\n");
			continue;
		}
		unsigned char *payload = receivebuf + 14;
		if(payload[0] != 0)
		{
			fprintf(stderr, "Wrong type, ignore\n");
			continue;
		}
		if(payload[1] != 0)
		{
			fprintf(stderr, "Wrong version, ignore\n");
			continue;		
		}
		if(ntohs(*((uint16_t *)(payload + 2)))	!= 24)
		{
			fprintf(stderr, "Wrong response length, ignore (Received %hu)\n", ntohs(*((uint16_t *)(payload + 2))));
			continue;
		}
		if(*((uint32_t *) (payload + 4)) != TID)
		{
			fprintf(stderr, "Wrong Transaction ID, ignore\n");
			continue;
		}

		printf("Received SDS reply\n");
		uint16_t TTL = ntohs(*((uint16_t *)(payload + 26)));	//TTL
		uint32_t port = ntohl(*((uint32_t *) (payload + 28)));	//Port
		memcpy(smp_ep_eth, (payload + 20), 6);		//Copy ethernet address
		
		printf("Attempting to add remote entry\n");
		pm_add_entry(ep->serviceID, ep->r, smp_ep_eth, port, TTL, 0);	//Register with EP port mapper
		printf("Added remote entry\n");

		printf("Received SDS reply : TTL=%hu, port=%u, MAC=%s\n", TTL, port, smp_ep_eth);
		free(receivebuf);	//Done with the buffer
		close(sockfd);	
		return 0;
		//TODO Timeout implementation?
	}

}


int socket_smp(const struct smp_ep *ep, socklen_t addrlen) 
                    //Returns: socket descriptor of end point on success,
                    //−1 on error, −2 on duplicate request
{
    struct smp_ep_eth dest;    //Destination information, obtained either from
                        //EP port mapper (if cached) or directory service
    int pm_result;        //To check if EP port mapper lookup is successful                        
    int pm_port_result; //To check if port binding was succesfully
    int directory_lookup_result;

	uint8_t my_mac[6];
	int my_port;
	
	struct ifreq if_idx;
	struct ifreq if_mac; 
	int sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_SMP_TYPE));
	  
    /* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Get own MAC address
    my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
	
    /* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	close(sockfd);
    //First look up EP port mapper 
    pm_result = pm_lookup(ep, sizeof(struct smp_ep), &dest, sizeof(struct smp_ep_eth));   //Should tell us if mapping is cached or not
        
    if(pm_result == SUCCEEDED)
    {
        int fd = socket(AF_PACKET, SOCK_RAW, htons(ETH_SMP_TYPE));
        //dest now contains required information for raw socket creation
        //Randomly generate a 32 bit port number and register with port mapper
        //Repeat until succesfully registered
        do
        {
            my_port = rand() % 4294967296;      //Random number from 0 to 2^32 - 1 (DOUBLE CHECK)
            printf("Attempting to add client entry\n");
            pm_port_result = pm_add_entry(ep->serviceID, CLIENT, my_mac, my_port, 600, 1);
            //Register with ep's service ID, role as client, local MAC, random port, TTL 0 (use default), and authoritative (from local process)
            if(pm_port_result == DUPLICATE)	//Two clients attempting to use same EP, reject
            {
            	return -1;
            }
        }
        while(pm_port_result == FAILED);    //Repeat until successful registration
        //Finished registration, now exit
        return fd;
    }
    
    //Entry not cached in EP port mapper
    //Run lookup on directory service
    directory_lookup_result = directory_smp(ep, sizeof(struct smp_ep) , dest.eth_addr, sizeof(dest.eth_addr)); 
    
    if(directory_lookup_result == SUCCEEDED)
    {
        //smp_ep_eth now contains required information for raw socket creation
        int fd = socket(AF_PACKET, SOCK_RAW, htons(ETH_SMP_TYPE));
        //Randomly generate a 32 bit port number and register with port mapper
        //Repeat until succesfully registered
        do
        {
            my_port = rand() % 4294967296;      //Random number from 0 to 2^32 - 1
            printf("Attempting to add client entry\n");
            pm_port_result = pm_add_entry(ep->serviceID, CLIENT, my_mac, my_port, 600, 1);
            //Register with ep's service ID, role as client, local MAC, TTL 0 (use default), and authoritative (from local process)
            if(pm_port_result == DUPLICATE)	//Two clients attempting to use same EP, reject
            {
            	close(fd);
            	return -1;
            }
        }
        while(pm_port_result == FAILED);    //Repeat until successful registration
        //Finished registration succesfully, now exit
        
        return fd;
    }
    //Both PM lookup and directory lookup failed, return error
    
    return FAILED;
    
}        

int sendto_smp(int smpfd,const void *buf,size_t bytes,
                const struct smp_ep *to,socklen_t addrlen) //Returns: number of bytes written on success, −1 on error                                                
{
    size_t total_size = bytes + 12 + 14;
    unsigned char* sendbuf = (unsigned char *) malloc(total_size); //bytes = payload size, 12 = SMP header size, 14 = Ethernet offset //TODO use macros for cleaner code
    uint32_t src_port, dest_port;       //To be obtained from EP mapper
    uint8_t my_mac[6];
    int sendto_ret;
    uint16_t payload_type;
    struct smp_ep_eth dest;
    
    struct ifreq if_idx;
	struct ifreq if_mac;   
	struct smp_ep my_ep;
	struct smp_ep_eth my_ep_eth;
	memcpy(my_ep.serviceID, to->serviceID, 10);

	
	my_ep.r = CLIENT;		

	pm_lookup(&my_ep, sizeof(struct smp_ep), &my_ep_eth, sizeof(struct smp_ep_eth));	//Look for local client
	//depending on SID
    /* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Get own MAC address
	my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
	
    //TODO    get_dest_mac(dest_mac); 
    //Get from the port mapper? How?
	//Check if mapping still valid in EP port mapper
	int  pm_result = pm_lookup(to, sizeof(struct smp_ep), &dest, sizeof(struct smp_ep_eth));   //Should tell us if mapping is cached or not
	if(pm_result == FAILED)	//Entry expired, run SDS lookup again
	{
		printf("Send:Not cached, looking up directory now\n");
		directory_smp(to, sizeof(struct smp_ep) , dest.eth_addr, sizeof(dest.eth_addr)); 
		//Here, we're assuming SDS entries will always remain present, and are never removed
	}
	pm_lookup(to, sizeof(struct smp_ep), &dest, sizeof(struct smp_ep_eth));   //Should be found now
	
    payload_type = ETH_SMP_TYPE;  //Experimental Ethernet payload
    
    memcpy((char *)sendbuf, (unsigned char *)dest.eth_addr, 6); //Copy destination MAC
    memcpy(((char *)sendbuf) + 6, (unsigned char *)my_mac, 6);   //Copy source MAC
    *((uint16_t *) (((char *)sendbuf) + 12)) = htons(0x88b5);	
    *((uint32_t *) (((char *)sendbuf+14))) = 0;	//Version
    *((uint32_t *) (((char *)sendbuf+15))) = 0;	//Profile
    *((uint16_t *) (((char *)sendbuf+16))) = htons(bytes);
    *((uint32_t *) (((char *)sendbuf) + 18)) = htonl(dest.port);	//Copy destination port
    *((uint32_t *) (((char *)sendbuf)+22)) = htonl(my_ep_eth.port);	//Copy source port
 
       memcpy(((char *)sendbuf) + 26, buf, bytes);  //Copy payload 
    /* Destination address */
    struct sockaddr_ll socket_address;
    
    /* Index of the network device */
    socket_address.sll_ifindex = if_idx.ifr_ifindex;
    /* Address length*/
    socket_address.sll_halen = ETH_ALEN;
    /* Destination MAC */
    socket_address.sll_addr[0] = dest.eth_addr[0];
    socket_address.sll_addr[1] = dest.eth_addr[1];
    socket_address.sll_addr[2] = dest.eth_addr[2];
    socket_address.sll_addr[3] = dest.eth_addr[3];
    socket_address.sll_addr[4] = dest.eth_addr[4];
    socket_address.sll_addr[5] = dest.eth_addr[5];
    /* Send packet */
    if ((sendto_ret = sendto(smpfd, sendbuf, total_size , 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll))) < 0)
        printf("Send failed\n");
        
    return sendto_ret - 26;        
}                

int recvfrom_smp(int smpfd, void *buf, size_t bytes,
                const struct smp_ep *from, socklen_t *addrlen) //Returns: number of bytes read on success, −1 on error
{
	//TODO Private service ID generation goes here?
	
	int bufsize = bytes + 12 + 14; 	//data + SMP header + Eth header
	unsigned char *receivebuf = (unsigned char *) malloc(bufsize);

	int received;
	uint16_t payload_length;
	uint32_t port;
	
	uint8_t my_mac[6];
	struct ifreq if_idx;
	struct ifreq if_mac; 
	struct smp_ep_eth src;
	struct smp_ep_eth my_ep_eth;
	
	struct smp_ep my_ep;
	strcpy(my_ep.serviceID, from->serviceID);
	my_ep.r = CLIENT;		
	
	 /* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Get own MAC address
    	my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
	
	
	
	
	//TODO Look up port mapper to find out port?
	pm_lookup(&my_ep, sizeof(struct smp_ep), &my_ep_eth, sizeof(struct smp_ep_eth));
	//Check if mapping still valid in EP port mapper
	int  pm_result = pm_lookup(from, sizeof(struct smp_ep), &src, sizeof(struct smp_ep_eth));   //Should tell us if mapping is cached or not
	if(pm_result == FAILED)	//Entry expired, run SDS lookup again
	{
		directory_smp(from, sizeof(struct smp_ep) , src.eth_addr, sizeof(src.eth_addr)); 
		//Here, we're assuming SDS entries will always remain present, and are never removed
	}
	pm_lookup(from, sizeof(struct smp_ep), &src, sizeof(struct smp_ep_eth));   //Should tell us if mapping is cached
	while(1)
	{
		memset(receivebuf, 0, 46);	//Clear buffer
		received = recvfrom(smpfd, receivebuf, bufsize, 0, NULL, NULL);
		printf("Received %d bytes\n", received);
		
		if(receivebuf[0] != my_mac[0] ||
			receivebuf[1] != my_mac[1] ||
			receivebuf[2] != my_mac[2] ||
			receivebuf[3] != my_mac[3] ||
			receivebuf[4] != my_mac[4] ||
			receivebuf[5] != my_mac[5])	//Not meant for me, ignore
		{
			fprintf(stderr, "Packet not meant for me\n");
			continue;
		}
		if(memcmp(receivebuf + 6, src.eth_addr, 6) != 0)
		{
			fprintf(stderr, "Received from wrong MAC\n");
			fprintf(stderr, "Received from : %x:%x:%x:%x:%x:%x:\n", receivebuf[6], receivebuf[7], receivebuf[8], receivebuf[9], receivebuf[10], receivebuf[11]);
			fprintf(stderr, "Expected from : %x:%x:%x:%x:%x:%x:\n", src.eth_addr[0], src.eth_addr[1], src.eth_addr[2], src.eth_addr[3], src.eth_addr[4], src.eth_addr[5]);
			continue;
		}
		unsigned char *smp_header = receivebuf + 14;
		if(smp_header[0] != 0)
		{
			fprintf(stderr, "Wrong version, ignore\n");
			continue;
		}
		if(smp_header[1] != 0)
		{
			fprintf(stderr, "Wrong profile, ignore\n");
			continue;		
		}
		if(ntohl(*((uint32_t *)(smp_header + 8))) != src.port)
		{
			fprintf(stderr, "Sent from wrong port, ignore\n");
			fprintf(stderr, "Expected : %u\n", src.port);
			fprintf(stderr, "Actually : %u\n", ntohl(*((uint32_t *)(smp_header + 8)))); 
			continue;				
		}

		payload_length = *((uint16_t *) (smp_header + 2));
		payload_length = ntohs(payload_length);
		uint32_t dest_port = *((uint32_t *) (smp_header + 4));
		dest_port = ntohl(dest_port);
		
		if(dest_port != my_ep_eth.port)	//Received on wrong port
		{
			fprintf(stderr, "Received on wrong port, ignore\n");
			fprintf(stderr, "Expected : %u\n", my_ep_eth.port);
			fprintf(stderr, "Actually : %u\n", dest_port);
			continue;
		}
		
		//Now pass data to application
		unsigned char *payload = smp_header + 12;
		memcpy((unsigned char *)buf, payload, payload_length);	//Copy data to application's buffer
		
		printf("Received %hu bytes of payload\n", payload_length);
		break;
		//TODO Timeout implementation?
	}
	free(receivebuf);
	return payload_length;
}

int recvfrom_smp_server(int server_fd, char *buf, unsigned int bytes, uint32_t server_port, struct smp_ep *client_ep)
{
	unsigned char *receivebuf = (char *)malloc(bytes + 26);
	int received;
	uint16_t payload_length;
	int port = server_port;
	uint8_t my_mac[6];
	struct ifreq if_idx;
	struct ifreq if_mac; 
	struct smp_ep_eth client_ep_eth;
	int i;
	uint8_t client_eth[6];
	
	int smpfd=socket(AF_PACKET, SOCK_RAW, htons(ETH_SMP_TYPE));
	 /* Get the index of the interface to receive on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to receive on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(smpfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Get own MAC address
    	my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];

	
	close(smpfd);
	while(1)
	{
		received = read(server_fd, receivebuf, bytes + 26);
		if(received < 26)
		{
			fprintf(stderr, "Frame too short\n");
			continue;
		}
		if(receivebuf[0] != my_mac[0] ||
			receivebuf[1] != my_mac[1] ||
			receivebuf[2] != my_mac[2] ||
			receivebuf[3] != my_mac[3] ||
			receivebuf[4] != my_mac[4] ||
			receivebuf[5] != my_mac[5])	//Not meant for me, ignore
		{
			fprintf(stderr, "Packet not meant for me\n");
			continue;
		}
		//Copy sender MAC into client_ep_eth
		memcpy(client_ep_eth.eth_addr, receivebuf + 6, 6);
		memcpy(client_eth, receivebuf + 6, 6);
		
		unsigned char *smp_header = (unsigned char *)(receivebuf+14);
		payload_length = *((uint16_t *) (smp_header + 2));
		payload_length = ntohs(payload_length);
		uint32_t dest_port = *((uint32_t *) (smp_header + 4));
		dest_port = ntohl(dest_port);
		uint32_t src_port = ntohl(*((uint32_t *)(smp_header + 8)));
		client_ep_eth.port = src_port;
		
		if(dest_port != port)	//Received on wrong port
		{
			fprintf(stderr, "Received on wrong port, ignore\n");
			continue;
		}
		
		memcpy(buf, smp_header+12 ,payload_length);	//Copy payload to application buffer
		client_ep->r = CLIENT;
		for(i = 0; i < 5; i++)
			client_ep->serviceID[i] = 'z';		//First 5 characters are 'z'
		while(1)
		{
			for(i = 5; i < 10; i++)
			{
				client_ep->serviceID[i] = (rand() % 26)	+ 'a';	//Random character from a-z
			}
			if(pm_lookup(client_ep, 
				sizeof(struct smp_ep), 
				&client_ep_eth, 
				sizeof(struct smp_ep_eth)) == FAILED)	//Safe to add
			{
				pm_add_entry(client_ep->serviceID, 
						client_ep->r,  
						client_eth, 
						src_port, 
						10, 
						0);		//Add entry to EP 
				break;	//Exit loop
			}		
		}
		
		free(receivebuf);
		return payload_length;
	}
	
	//Repeatedly read from server_fd socket until matching port, MAC is found
	
	//Update client_ep data for application to use while replying
	
	//Pass this message to application
}

int sendto_smp_server(int server_fd, char *buf, unsigned int bytes, uint32_t server_port, struct smp_ep *client_ep)
{
size_t total_size = bytes + 12 + 14;
    unsigned char* sendbuf = (unsigned char *) malloc(total_size); //bytes = payload size, 12 = SMP header size, 14 = Ethernet offset //TODO use macros for cleaner code
    uint32_t src_port, dest_port;       //To be obtained from EP mapper
    uint8_t my_mac[6];
    int sendto_ret;
    uint16_t payload_type;
    struct smp_ep_eth dest;
    
    struct ifreq if_idx;
	struct ifreq if_mac;   
	
	
	//depending on SID
    /* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(server_fd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	    
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(server_fd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");
	    
	//Get own MAC address
	my_mac[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
	my_mac[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
	my_mac[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
	my_mac[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
	my_mac[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
	my_mac[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
	
	
    //TODO    get_dest_mac(dest_mac); 
    //Get from the port mapper? How?
	//Check if mapping still valid in EP port mapper
	int  pm_result = pm_lookup(client_ep, sizeof(struct smp_ep), &dest, sizeof(struct smp_ep_eth));   //Should tell us if mapping is cached or not
	if(pm_result == FAILED)	//Entry expired, run SDS lookup again
	{
		printf("Send:Not cached, looking up directory now\n");
		directory_smp(client_ep, sizeof(struct smp_ep) , dest.eth_addr, sizeof(dest.eth_addr)); 
		//Here, we're assuming SDS entries will always remain present, and are never removed
	}
	pm_lookup(client_ep, sizeof(struct smp_ep), &dest, sizeof(struct smp_ep_eth));   //Should be found now
	
    payload_type = ETH_SMP_TYPE;  //Experimental Ethernet payload
    
    memcpy((char *)sendbuf, (unsigned char *)dest.eth_addr, 6); //Copy destination MAC
    memcpy(((char *)sendbuf) + 6, (unsigned char *)my_mac, 6);   //Copy source MAC
    *((uint16_t *) (((char *)sendbuf) + 12)) = htons(0x88b5);	
    *((uint32_t *) (((char *)sendbuf+14))) = 0;	//Version
    *((uint32_t *) (((char *)sendbuf+15))) = 0;	//Profile
    *((uint16_t *) (((char *)sendbuf+16))) = htons(bytes);
    *((uint32_t *) (((char *)sendbuf) + 18)) = htonl(dest.port);	//Copy destination port
    *((uint32_t *) (((char *)sendbuf)+22)) = htonl(server_port);	//Copy source port
 
       memcpy(((char *)sendbuf) + 26, buf, bytes);  //Copy payload 
    /* Destination address */
    struct sockaddr_ll socket_address;
    
    /* Index of the network device */
    socket_address.sll_ifindex = if_idx.ifr_ifindex;
    /* Address length*/
    socket_address.sll_halen = ETH_ALEN;
    /* Destination MAC */
    socket_address.sll_addr[0] = dest.eth_addr[0];
    socket_address.sll_addr[1] = dest.eth_addr[1];
    socket_address.sll_addr[2] = dest.eth_addr[2];
    socket_address.sll_addr[3] = dest.eth_addr[3];
    socket_address.sll_addr[4] = dest.eth_addr[4];
    socket_address.sll_addr[5] = dest.eth_addr[5];
    /* Send packet */
    if ((sendto_ret = sendto(server_fd, sendbuf, total_size , 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll))) < 0)
        printf("Send failed\n");
        
    return sendto_ret - 26;    	
}

int close_smp(int smpfd)                                   //Returns: 0 on success, −1 on error
{
	return close(smpfd);
}
/*
int main()
{
	init_random_generator();
	close(3);
	struct smp_ep ep;
	ep.r = SERVER;
	strcpy(ep.serviceID, "dummycheck");
	struct smp_ep_eth tmp;
	
	int ret = directory_smp (&ep, sizeof(ep), (char *)&tmp, 14);
	//int sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_SMP_TYPE));
      	//sendto_smp(sockfd, "Help Me", 7, NULL, 12);
      	//close(sockfd);
}
                
*/            
