#ifndef SMP_LIBRARY_H
#define SMP_LIBRARY_H

int socket_smp(const struct smp_ep *ep, socklen_t addrlen); //Returns: socket descriptor of end point on success,
                                                            //−1 on error, −2 on duplicate request
                                                                                                                        
int close_smp(int smpfd);                                   //Returns: 0 on success, −1 on error

int sendto_smp(int smpfd,const void *buf,size_t bytes,
                const struct smp_ep *to,socklen_t addrlen); //Returns: number of bytes written on success, −1 on error
        
int sendto_smp_server(int server_fd, char *buf, unsigned int bytes, uint32_t server_port, struct smp_ep *client_ep);
                
int recvfrom_smp(int smpfd,void *buf,size_t bytes,
                const struct smp_ep *from,socklen_t *addrlen); //Returns: number of bytes read on success, −1 on error

int recvfrom_smp_server(int server_fd, char *buf, unsigned int bytes, uint32_t server_port, struct smp_ep *client_ep);

int directory_smp(const struct smp_ep *ep, socklen_t addrlen,
                    char *smp_ep_eth, socklen_t ethlen);  //Returns: 0 on success, −1 on error
                    

int pm_lookup(const struct smp_ep *ep, socklen_t addrlen,
		struct smp_ep_eth *ep_addr, socklen_t ethlen);	//Checks for cached entries in EP mapper, 0 if uncached, 1 if cached                    
		

int pm_remove_client(const struct smp_ep *ep, socklen_t addrlen);	//Remove local mapping from EP mapper, returns 0 on success, 1 on failure
         
int pm_add_entry(const char *serviceID, uint16_t role,  uint8_t eth_addr[6], uint32_t port, uint16_t TTL, char auth);	//Inserts entry with required mappings, returns 0 on success 
      
void init_random_generator();    //Initialize the random number generator, 
                                //needed for random port number generation at 
                                //client side

#endif
